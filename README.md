# The Kuartz adventures 🤺

![Ready for adventure!](https://media.giphy.com/media/dZRCL4lz0lZKdlckNp/giphy.gif)

## Objectives

Go from level 1 to level 14 to get ready for Freelancing at Kuartz 💪

## Levels

### [🥚 Level 1](/levels/level_1.md)

App creation with **Devise** and **I18n** 🔧

### [🐣 Level 2](/levels/level_2.md)

Good style and code workflow (**Rubocop** + **Gitlab**) 🛂

### [🐥 Level 3](/levels/level_3.md)

Let's get rid of erb 👉 Slim FTW ✂

### [🐁 Level 4](/levels/level_4.md)

Image upload with Cloudinary 🖼

### [🐿 Level 5](/levels/level_5.md)

Advanced Rails arragement (services & decorators) 🗂

### [🦔 Level 6](/levels/level_6.md)

Transactional emails ready for production with **MailJet**! 📨

### [🐇 Level 7](/levels/level_7.md)

You are now using TDD ❌✔

### [🐐 Level 8](/levels/level_8.md)

Welcome to the javascript playground with **flatpickr** 📅

### [🐑 Level 9](/levels/level_9.md)

Advanced JS: **Stimulus** 🔄

### [🐏 Level 10](/levels/level_10.md)

Pretty graphs with **Frappe Charts** 📊

### [🦅 Level 11](/levels/level_11.md)

Follow the JS rabbit hole: **dropzone.js** 🕳

### [🐘 Level 12](/levels/level_12.md)

Export in PDF 🖨

### [🐋 Level 13](/levels/level_13.md)

WTF is this SQL query 😳

### [🦄 Level 14](/levels/level_14.md)

Get paid with Stripe 💰
